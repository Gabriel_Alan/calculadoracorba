/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helloclient;

import HelloApp.Hello;
import HelloApp.HelloHelper;
import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;

/**
 *
 * @author gabri
 */
public class HelloClient {

    public static void main(String[] args) throws InvalidName {
        try{
       ORB orb = ORB.init(args, null);
       org.omg.CORBA.Object ref = orb.resolve_initial_references("NameService");
       NamingContextExt ncRef =  NamingContextExtHelper.narrow(ref);
       Hello hellobj = (Hello) HelloHelper.narrow(ncRef.resolve_str("ABC"));
       
       System.out.println("bem vindo");
       

       double r = hellobj.soma(5 , 5);
       System.out.println(r);
       
        } catch (Exception e){
            System.out.println("Exception" + e);
        }
    }
}
